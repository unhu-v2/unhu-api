package com.unhu.ms.userengagement.Service.User;


import com.unhu.ms.userengagement.DTO.ResponseDTO;

public interface FollowService {
    public ResponseDTO addFollower(String username, String followingUsername);
    public ResponseDTO removeFollower(String username, String followingUsername);
}
