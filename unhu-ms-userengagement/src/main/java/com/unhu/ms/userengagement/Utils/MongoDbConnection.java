package com.unhu.ms.userengagement.Utils;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoDatabase;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class MongoDbConnection {
    private final static Logger LOGGER = Logger.getLogger(MongoDbConnection.class.getName());
    public MongoClient mongoClient;
    private Properties mongoProperties;

    MongoDbConnection () throws IOException {
        this.mongoProperties = new Properties();
        String propertyFileName = "application.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertyFileName);

        if (inputStream == null) {
            throw new FileNotFoundException("Unable to locate Property File in the classpath");
        }

        mongoProperties.load(inputStream);
        this.mongoClient = new MongoClient( mongoProperties.getProperty("unhu.mongodb.host") , Integer.parseInt(mongoProperties.getProperty("unhu.mongodb.port")));
    }


    public MongoDatabase createConnection()  {
        String message = "";

        MongoCredential.createCredential(mongoProperties.getProperty("unhu.mongodb.username"), mongoProperties.getProperty("unhu.mongodb.database"),
                mongoProperties.getProperty("unhu.mongodb.password").toCharArray());
        MongoDatabase database = mongoClient.getDatabase(mongoProperties.getProperty("unhu.mongodb.database"));

        LOGGER.info("Connected to the database successfully");
        return database;
    }

    public void close(){
        mongoClient.close();
    }
}
