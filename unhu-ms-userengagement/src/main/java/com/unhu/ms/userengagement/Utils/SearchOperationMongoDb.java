package com.unhu.ms.userengagement.Utils;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SearchOperationMongoDb {
    private MongoDatabase mongoDatabase;
    private MongoDbConnection mongoDbConnection;
    public SearchOperationMongoDb()  {
        try {
            this.mongoDbConnection = new MongoDbConnection();
            this.mongoDatabase = mongoDbConnection.createConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Check if the document exists in the Database or not.
    public boolean  searchForExistingProfile(Map<String, Object> queryMap, String collectionName){
        MongoCollection<Document> documemts =  mongoDatabase.getCollection(collectionName);
        Map<String, Object> checkMap = new HashMap<>();
        checkMap.put("username", queryMap.get("username"));
        BasicDBObject query = new BasicDBObject(checkMap);
        FindIterable<Document> iterable =  documemts.find(query);
        boolean returnValue = iterable.first() == null;
        return returnValue;
    }

    //Validate Password from UI against that in Password
    public boolean  validateEmailPassword(Map<String, Object> queryMap, String collectionName){
        MongoCollection<Document> documents =  mongoDatabase.getCollection(collectionName);
        String [] emailPasswordValue = new String[2];
        String [] emailPasswordkey = new String[2];
        int i=0;
        for (Map.Entry<String, Object> entry : queryMap.entrySet()) {
            String k = entry.getKey();
            Object v = entry.getValue();
            emailPasswordkey[i] = k;
            emailPasswordValue[i] = (String) v;
            i++;
        }
        Bson filter = Filters.and(Filters.all(emailPasswordkey[0], emailPasswordValue[0]),
                                  Filters.all(emailPasswordkey[1], emailPasswordValue[1]));
        MongoCursor<Document> cursor = documents.find(filter).iterator();
        return cursor.hasNext();
    }


}
