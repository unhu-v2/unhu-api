package com.unhu.ms.userengagement.ServiceImpl.User;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.unhu.ms.userengagement.DTO.User.LoginDTO;
import com.unhu.ms.userengagement.Utils.InsertOperationMongoDb;
import com.unhu.ms.userengagement.DTO.User.RegistrationDTO;
import com.unhu.ms.userengagement.DTO.ResponseDTO;
import com.unhu.ms.userengagement.DTO.User.UpdatePasswordDTO;
import com.unhu.ms.userengagement.Service.User.UserEngagementService;
import com.unhu.ms.userengagement.Utils.SearchOperationMongoDb;
import com.unhu.ms.userengagement.Utils.UpdateOperationMongoDb;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

public class UserEngagementImpl implements UserEngagementService {
    private String message = "";
    private String responseCode = "";
    private Map<String, Object> map;
    private final static Logger LOGGER = Logger.getLogger(UserEngagementImpl.class.getName());

    @Override
    public ResponseDTO registerUser(RegistrationDTO registrationDTO) throws IOException {

        ObjectMapper oMapper = new ObjectMapper();
        map = getMap(registrationDTO);
        InsertOperationMongoDb insertOperationMongoDb = new InsertOperationMongoDb();
        if(insertOperationMongoDb.insertDataMongoDb(map, "unhu_users")){
            message = "Uploaded the User Information to Database";
            responseCode = "200";
        }
        else{
            message = "Record Exists in the Database";
            responseCode = "409";
        }

        return new ResponseDTO(responseCode, message);
    }

    @Override
    public ResponseDTO login(LoginDTO loginDTO)  {
        ObjectMapper oMapper = new ObjectMapper();
        map = getMap(loginDTO);
        SearchOperationMongoDb searchOperationMongoDb = new SearchOperationMongoDb();
        if(searchOperationMongoDb.validateEmailPassword(map, "unhu_users")){
            message = "User Login Successful";
            responseCode = "200";
        }
        else{
            message = "Unable to validate user";
            responseCode = "401";
        }
        return new ResponseDTO(responseCode, message);
    }

    @Override
    public ResponseDTO changePassword(UpdatePasswordDTO updatePasswordDTO) {
        map = getMap(updatePasswordDTO);
        String newValue = map.keySet().iterator().next();
        UpdateOperationMongoDb mongoDbOperations =
                new UpdateOperationMongoDb("unhu_users", map);
        if(mongoDbOperations.updatePassword()){
            message = "Password Changed for the user";
            responseCode = "201";
        }
        else{
            message = "Failed to change password";
            responseCode = "500";
        }
        return new ResponseDTO(responseCode, message);
    }

    public Map<String, Object> getMap(Object object){
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> map = oMapper.convertValue(object, Map.class);
        return map;
    }

}
