package com.unhu.ms.userengagement.DTO.User;

import java.util.List;

public class FollowerList {
    public List<String> getFollowerNames() {
        return followerNames;
    }

    public void setFollowerNames(List<String> followerNames) {
        this.followerNames = followerNames;
    }

    List<String> followerNames;


}
