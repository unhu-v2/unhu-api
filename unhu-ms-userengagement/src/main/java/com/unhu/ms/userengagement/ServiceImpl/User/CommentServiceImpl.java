package com.unhu.ms.userengagement.ServiceImpl.User;



import com.unhu.ms.userengagement.DTO.ResponseDTO;
import com.unhu.ms.userengagement.Service.User.CommentService;
import com.unhu.ms.userengagement.Utils.UpdateOperationMongoDb;

import java.util.Map;

public class CommentServiceImpl implements CommentService {
    String message;
    String responseCode;

    @Override
    public ResponseDTO addComment(String postUid, String username, String description) {
        UpdateOperationMongoDb updateOperationMongoDb = new UpdateOperationMongoDb("unhu_posts");
        if(updateOperationMongoDb.addComment(postUid, username, description)){
            message = "Added a comment to the post";
            responseCode = "200";
        }
        else{
            message = "Unable to add a comment to the post";
            responseCode = "500";
        }
        return new ResponseDTO(message, responseCode);
    }

    @Override
    public Map<String, Object> getComments(String postUid) {
        UpdateOperationMongoDb searchOperationMongoDb = new UpdateOperationMongoDb("unhu_posts");
        return searchOperationMongoDb.getComments(postUid);
    }

}
