package com.unhu.ms.userengagement.DTO.User;

import java.util.ArrayList;

public class Following {
    public FollowingList following_list;
    public String following_count;


    public String getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(String following_count) {
        this.following_count = following_count;
    }

    public FollowingList getFollowing_list() {
        return following_list;
    }

    public void setFollowing_list(FollowingList following_list) {
        this.following_list = following_list;
    }
}
