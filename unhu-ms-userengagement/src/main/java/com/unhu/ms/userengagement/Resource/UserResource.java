package com.unhu.ms.userengagement.Resource;

import com.google.gson.Gson;
import com.unhu.ms.userengagement.DTO.Likes;
import com.unhu.ms.userengagement.DTO.PostDTO;
import com.unhu.ms.userengagement.DTO.ResponseDTO;
import com.unhu.ms.userengagement.DTO.User.*;
import com.unhu.ms.userengagement.ServiceImpl.User.CommentServiceImpl;
import com.unhu.ms.userengagement.ServiceImpl.User.LikeServiceImpl;
import com.unhu.ms.userengagement.ServiceImpl.User.PostServiceImpl;
import com.unhu.ms.userengagement.ServiceImpl.User.UserEngagementImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/api/v1/user", path = "/api/v1/user")
public class UserResource {
    private final static Logger LOGGER = Logger.getLogger(UserResource.class.getName());

    @ApiOperation(value = "Register a new user to Unhu")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 200, message = "New User registered with Unhu"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @PostMapping(value = "/register", consumes = "application/json")
    public ResponseDTO register(@RequestBody RegistrationDTO data) {
        UserEngagementImpl registration = new UserEngagementImpl();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        try {
            data.setBio("");
            data.followers.setFollower_count("0");
            data.followers.setFollower_list(data.getFollowers().getFollower_list());
            data.following.setFollowing_count("0");
            data.following.setFollowing_list(data.getFollowing().getFollowing_list());
            data.setLink("");
            data.setPosts(new Posts());
            data.setShelf(new Shelf());
            data.setUser_since(dtf.format(now));
            data.setSmarthub_link("");
            return registration.registerUser(data);
        }
         catch (IOException e) {
             LOGGER.log(Level.SEVERE, "Unable to register: " + e.getMessage());
        }

        return null;
    }

    @ApiOperation(value = "Login Validation for Unhu user")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 200, message = "Authorisation Successful"),
                    @ApiResponse(code = 401, message = "Unauthorised Access"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @PostMapping(value = "/login", consumes = "application/json")
    public ResponseDTO login(@RequestBody LoginDTO data) {
        UserEngagementImpl userLogin = new UserEngagementImpl();
        return userLogin.login(data);
    }

    @ApiOperation(value = "Update or Change password")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 201, message = "Updated Password for the user"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @PutMapping(value = "/change_password", consumes = "application/json")
    public ResponseDTO changePassword(@RequestBody UpdatePasswordDTO data) {
        UserEngagementImpl changePwd = new UserEngagementImpl();
        return changePwd.changePassword(data);
    }

    @ApiOperation(value = "Creates a new post for a user")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 200, message = "New post added for the user"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @PutMapping(value = "/post", consumes = "application/json")
    public ResponseDTO uploadPost(@RequestBody PostDTO data) {
        PostServiceImpl post = new PostServiceImpl();
        String uniqueID = UUID.randomUUID().toString();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        data.setPost_uid(uniqueID);
        data.setTimestamp(now.toString());
        data.likes.setLikes_count("0");
        data.likes.setLiked_by(new ArrayList<>());
        data.comments.setComment_count("0");
        data.comments.setComment_likes("0");
        data.comments.setComment_description(new ArrayList<>());
        data.setShelved_by(new ArrayList<>());
        return post.addPost(data);
    }

    @ApiOperation(value = "Likes a post given the post_uid")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 200, message = "New Like added to the post with post_uid"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @PostMapping(value = "/like/{post_uid}")
    public ResponseDTO likePost(@PathVariable String post_uid, @RequestParam String username) {
        LikeServiceImpl like = new LikeServiceImpl();
        return like.addLike(post_uid, username);
    }

    @ApiOperation(value = "Unlikes a post given the post_uid")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 200, message = "New Like added to the post with post_uid"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @DeleteMapping(value = "/unlike/{post_uid}")
    public ResponseDTO unLikePost(@PathVariable String post_uid, @RequestParam String username) {
        LikeServiceImpl like = new LikeServiceImpl();
        return like.removeLike(post_uid, username);
    }

    @ApiOperation(value = "Gets the number of likes of a post given the post_uid")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 200, message = "Gets the number of likes for a post with post_uid"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @GetMapping(value = "/likes/{post_uid}", produces = "application/json")
    public Likes getLikesCountAndLikesList(@PathVariable String post_uid) {
        LikeServiceImpl like = new LikeServiceImpl();
        return like.getLikesAndLikeList(post_uid);
    }

    @ApiOperation(value = "Add a new Comment to the post")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 200, message = "Succesfully commented on a post"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @PostMapping(value = "/comment/{post_uid}")
    public ResponseDTO addComments(@PathVariable String post_uid, @RequestParam String username, @RequestParam String description) {
        CommentServiceImpl commentService = new CommentServiceImpl();
        return commentService.addComment(post_uid, username, description);
    }

    @ApiOperation(value = "Returns a json with Comments information")
    @ApiResponses(
            value= {
                    @ApiResponse(code = 200, message = "Succesfully retreived a comment of a post"),
                    @ApiResponse(code = 500, message = "Internal Server Error")
            }
    )
    @GetMapping(value = "/comment/{post_uid}")
    public Map<String, Object> getComments(@PathVariable String post_uid) {
        CommentServiceImpl commentService = new CommentServiceImpl();
        return commentService.getComments(post_uid);
    }

}
