package com.unhu.ms.userengagement.Utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import com.unhu.ms.userengagement.DTO.Likes;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

public class UpdateOperationMongoDb {
    private final static Logger LOGGER = Logger.getLogger(InsertOperationMongoDb.class.getName());
    private MongoDatabase mongoDatabase;
    private MongoDbConnection mongoDbConnection;
    private String collection;
    private Map<String, Object> map;
    public UpdateOperationMongoDb(String collection, Map<String, Object> map){
        try {
            this.mongoDbConnection = new MongoDbConnection();
            this.mongoDatabase = mongoDbConnection.createConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.collection = collection;
        this.map = map;
    }

    public UpdateOperationMongoDb(String collection){
        this.collection = collection;
        try {
            this.mongoDbConnection = new MongoDbConnection();
            this.mongoDatabase = mongoDbConnection.createConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean updatePassword(){
        MongoCollection<Document> gradesCollection = mongoDatabase.getCollection(collection);
        String [] emailPasswordValue = new String[2];
        String [] emailPasswordkey = new String[2];
        int i=0;
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String k = entry.getKey();
            Object v = entry.getValue();
            emailPasswordkey[i] = k;
            emailPasswordValue[i] = (String) v;
            i++;
        }
        Bson filter = Filters.and(Filters.all(emailPasswordkey[1], emailPasswordValue[1]));
        Bson updateOperation = set(emailPasswordkey[0], emailPasswordValue[0]);
        UpdateResult updateResult = gradesCollection.updateOne(filter, updateOperation);
        return updateResult.wasAcknowledged();
    }

    public boolean updateUserPosts(String username){
        MongoCollection<Document> userCollection = mongoDatabase.getCollection(collection);
        List<Object> documentList = null;
        try (MongoCursor<Document> cursor = userCollection.find().iterator()){
            while (cursor.hasNext()){
                documentList = new ArrayList<Object>(cursor.next().values());
            }
        }

        int updateIndex = 7;

        Object userPosts = documentList.get(updateIndex);
        Map<String, Object> postsMap= getMap(userPosts);
        List<String> post = (List)postsMap.get("postsList");
        if(post == null){
            post = new ArrayList<>();
        }
        post.add(map.get("post_uid").toString());
        postsMap.put("postsList", post);
        Bson filter = Filters.and(Filters.all("username", username));
        Bson updateOperation = set("posts", (Object) postsMap);
        UpdateResult updateResult = userCollection.updateOne(filter, updateOperation);
        return updateResult.wasAcknowledged();
    }

    public Likes getLikesAndLikesList(String postUid){
        MongoCollection<Document> postCollection = mongoDatabase.getCollection(collection);
        Map<String, Object> likeMap = getLikeMap(postUid, postCollection);
        Likes likes = new Likes();
        likes.liked_by = (List)likeMap.get("liked_by");
        likes.likes_count = (String)likeMap.get("likes_count");
        return likes;
    }

    public boolean removePostLike(String postUid, String username){
        MongoCollection<Document> postCollection = mongoDatabase.getCollection(collection);
        Map<String, Object> likeMap = getLikeMap(postUid, postCollection);
        List<String> likedBy = (List)likeMap.get("liked_by");
        if (likedBy.contains(username)){
            likedBy.remove(username);
        }
        else{
            LOGGER.info("User has not liked the post");
        }
        String likesCount = String.valueOf(likedBy.size());
        likeMap.put("liked_by", likedBy);
        likeMap.put("likes_count", likesCount);
        return updateMongoDocument(postUid, likeMap, postCollection);
    }

    public boolean addPostLike(String postUid, String username){
        MongoCollection<Document> postCollection = mongoDatabase.getCollection(collection);
        Map<String, Object> likeMap = getLikeMap(postUid, postCollection);
        List<String> likedByList = (List) likeMap.get("liked_by");

        if(likedByList == null){
            likedByList = new ArrayList<>();
        }

        if(!likedByList.contains(username)){
            likedByList.add(username);
        }

        String likesCount = String.valueOf(likedByList.size());
        likeMap.put("likes_count", likesCount);
        likeMap.put("liked_by", likedByList);
        return updateMongoDocument(postUid, likeMap, postCollection);
    }

    public boolean addUserFollowing(String username, String followingUsername){
        MongoCollection<Document> userCollection = mongoDatabase.getCollection(collection);
        UpdateResult updateResult = null;
        Bson whereQuery = Filters.all("username", username);
        FindIterable<Document> document = userCollection.find(whereQuery);
        Object userFollowing = document.first().get("following");
        Map<String, Object> followingMap = getMap(userFollowing);
        Map<String, Object> following = getMap(followingMap.get("following_list"));
        List<String> followingList = (List)following.get("followingList");
        if(!followingList.contains(followingUsername)) {
            followingList.add(followingUsername);
        }
        int count = followingList.size();
        String followingCount = String.valueOf(count);
        following.put("followingList", followingList);
        followingMap.put("following_list", following);
        followingMap.put("following_count", followingCount);
        Bson filter = Filters.and(Filters.all("username", username));
        Bson updateOperation = set("following", (Object) followingMap);
        updateResult = userCollection.updateOne(filter, updateOperation);
        mongoDbConnection.close();
        return updateResult.wasAcknowledged();

    }

    public boolean addUserFollower(String followingUsername, String username){
        MongoCollection<Document> userCollection = mongoDatabase.getCollection(collection);
        UpdateResult updateResult = null;
        Bson whereQuery = Filters.all("username", username);
        FindIterable<Document> document = userCollection.find(whereQuery);
        Object userFollower = document.first().get("followers");
        Map<String, Object> followerMap = getMap(userFollower);
        Map<String, Object> follower = getMap(followerMap.get("follower_list"));
        List<String> followerList = (List)follower.get("followerNames");

        if(followerList == null){
            followerList = new ArrayList<>();
        }
        if(!followerList.contains(username)){
            followerList.add(username);
        }
        int count = followerList.size();
        String followerCount = String.valueOf(count);
        follower.put("followerNames", followerList);
        followerMap.put("follower_list", follower);
        followerMap.put("follower_count", followerCount);
        Bson filter = Filters.and(Filters.all("username", followingUsername));
        Bson updateOperation = set("followers", (Object) followerMap);
        updateResult = userCollection.updateOne(filter, updateOperation);
        mongoDbConnection.close();
        return updateResult.wasAcknowledged();
    }

    public boolean removeUserFollowing(String username, String followingUsername){
        MongoCollection<Document> userCollection = mongoDatabase.getCollection(collection);
        UpdateResult updateResult = null;
        Bson whereQuery = Filters.all("username", username);
        FindIterable<Document> document = userCollection.find(whereQuery);
        Object userFollowing = document.first().get("following");
        Map<String, Object> followingMap = getMap(userFollowing);
        Map<String, Object> following = getMap(followingMap.get("following_list"));
        List<String> followingList = (List)following.get("followingList");
        if(followingList.contains(followingUsername)) {
            followingList.remove(followingUsername);
        }
        int count = followingList.size();
        String followingCount = String.valueOf(count);
        following.put("followingList", followingList);
        followingMap.put("following_list", following);
        followingMap.put("following_count", followingCount);
        Bson filter = Filters.and(Filters.all("username", username));
        Bson updateOperation = set("following", (Object) followingMap);
        updateResult = userCollection.updateOne(filter, updateOperation);
        mongoDbConnection.close();
        return updateResult.wasAcknowledged();

    }

    public boolean removeUserFollower(String followingUsername, String username){
        MongoCollection<Document> userCollection = mongoDatabase.getCollection(collection);
        UpdateResult updateResult = null;
        Bson whereQuery = Filters.all("username", username);
        FindIterable<Document> document = userCollection.find(whereQuery);
        Object userFollower = document.first().get("followers");
        Map<String, Object> followerMap = getMap(userFollower);
        Map<String, Object> follower = getMap(followerMap.get("follower_list"));
        List<String> followerList = (List)follower.get("followerNames");

        if(followerList.contains(username)){
            followerList.remove(username);
        }
        int count = followerList.size();
        String followerCount = String.valueOf(count);
        follower.put("followerNames", followerList);
        followerMap.put("follower_list", follower);
        followerMap.put("follower_count", followerCount);
        Bson filter = Filters.and(Filters.all("username", followingUsername));
        Bson updateOperation = set("followers", (Object) followerMap);
        updateResult = userCollection.updateOne(filter, updateOperation);
        mongoDbConnection.close();
        return updateResult.wasAcknowledged();
    }

    public boolean addComment(String postUid, String username, String description){
        MongoCollection<Document> postCollection = mongoDatabase.getCollection(collection);
        Map<String, Object> commentMap = getCommentMap(postUid, postCollection);
        Map<String, String> comment = new HashMap<>();
        comment.put("username", username);
        comment.put("comment_description", description);
        List<Object> listComments = (List) commentMap.get("comment_map");

        if(listComments == null){
            listComments = new ArrayList<>();
        }
        listComments.add(comment);
        String commentCount = String.valueOf(listComments.size());
        commentMap.put("comment_count", commentCount);
        commentMap.put("comment_map", listComments);
        return updateMongoDocument(postUid, commentMap, postCollection);
    }

    public Map<String, Object> getComments(String postUid){
        MongoCollection<Document> postCollection = mongoDatabase.getCollection(collection);
        Map<String, Object> commentMap = getCommentMap(postUid, postCollection);
        return commentMap;
    }



    private Map<String, Object> getCommentMap(String postUid, MongoCollection<Document> postCollection){
        Bson whereQuery = Filters.all("post_uid", postUid);
        FindIterable<Document> document = postCollection.find(whereQuery);
        Object commentObject = document.first().get("comments");
        Map<String, Object> commentMap = getMap(commentObject);
        return commentMap;
    }

    // Helper Functions to main Functions above

    private Map<String, Object> getLikeMap(String postUid, MongoCollection<Document> postCollection){
        Bson whereQuery = Filters.all("post_uid", postUid);
        FindIterable<Document> document = postCollection.find(whereQuery);
        Object likeObject = document.first().get("likes");
        Map<String, Object> likeMap = getMap(likeObject);
        return likeMap;
    }

    private boolean updateMongoDocument(String postUid, Map<String, Object> likeMap, MongoCollection<Document> postCollection){
        UpdateResult updateResult = null;

        Bson filter = Filters.and(Filters.all("post_uid", postUid));
        Bson updateOperation = set("likes", (Object) likeMap);
        updateResult = postCollection.updateOne(filter, updateOperation);
        mongoDbConnection.close();
        return updateResult.wasAcknowledged();
    }


    public Map<String, Object> getMap(Object object){
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> map = oMapper.convertValue(object, Map.class);
        return map;
    }

}
