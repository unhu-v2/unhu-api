package com.unhu.ms.userengagement.ServiceImpl.User;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unhu.ms.userengagement.DTO.PostDTO;
import com.unhu.ms.userengagement.DTO.ResponseDTO;
import com.unhu.ms.userengagement.Service.User.PostService;
import com.unhu.ms.userengagement.Utils.InsertOperationMongoDb;
import com.unhu.ms.userengagement.Utils.UpdateOperationMongoDb;


import java.io.IOException;
import java.util.Map;

public class PostServiceImpl implements PostService {

    @Override
    public ResponseDTO addPost(PostDTO data) {
        InsertOperationMongoDb insertOperationMongoDb = new InsertOperationMongoDb();
        String message = "";
        String responseCode = "";
        Map<String, Object> map = getMap(data);
        try {
            if(insertOperationMongoDb.insertDataMongoDb(map, "unhu_posts")){
                UpdateOperationMongoDb updateOperationMongoDb = new UpdateOperationMongoDb("unhu_users", map);
                if(updateOperationMongoDb.updateUserPosts(data.username)){
                    message = "Uploaded the post to Database";
                    responseCode = "200";
                }
                else{
                    message = "Uploaded the Post to DB but unable to add it to User posts";
                    responseCode = "500";
                }
            }
            else{
                message = "Failed to upload post to Database";
                responseCode = "409";
            }
            return new ResponseDTO(responseCode, message);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseDTO("500", e.getMessage());
        }
    }

    public Map<String, Object> getMap(Object object){
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> map = oMapper.convertValue(object, Map.class);
        return map;
    }
}
