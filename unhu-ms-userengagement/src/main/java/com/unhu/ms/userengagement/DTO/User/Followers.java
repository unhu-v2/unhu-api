package com.unhu.ms.userengagement.DTO.User;

import java.util.ArrayList;

public class Followers {

    public FollowerList follower_list;
    public String follower_count;

    public String getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(String follower_count) {
        this.follower_count = follower_count;
    }

    public FollowerList getFollower_list() {
        return follower_list;
    }

    public void setFollower_list(FollowerList follower_list) {
        this.follower_list = follower_list;
    }
}
