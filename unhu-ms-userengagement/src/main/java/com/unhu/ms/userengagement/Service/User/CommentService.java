package com.unhu.ms.userengagement.Service.User;

import com.unhu.ms.userengagement.DTO.ResponseDTO;
import java.util.Map;

public interface CommentService {
    ResponseDTO addComment(String username, String followingUsername, String description);
    Map<String, Object> getComments(String postUid);

}
