package com.unhu.ms.userengagement.DTO;

public class ResponseDTO {
    private String responseCode;
    private String message;

    public ResponseDTO(String responseCode, String message){
        this.responseCode = responseCode;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
