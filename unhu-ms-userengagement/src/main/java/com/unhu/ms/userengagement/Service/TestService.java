package com.unhu.ms.userengagement.Service;

import com.unhu.ms.userengagement.DTO.TestDTO;

public interface TestService {
    TestDTO testFunction(String name);
}
