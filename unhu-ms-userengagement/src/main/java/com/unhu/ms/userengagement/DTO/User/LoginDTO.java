package com.unhu.ms.userengagement.DTO.User;

import com.mongodb.lang.NonNull;

public class LoginDTO {
    @NonNull
    private String username;
    @NonNull
    private String password;



    @NonNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    @NonNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NonNull String username) {
        this.username = username;
    }
}
