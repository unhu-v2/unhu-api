package com.unhu.ms.userengagement.DTO;

public class TestDTO {
    private final long id;
    private final String content;

    public TestDTO(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
