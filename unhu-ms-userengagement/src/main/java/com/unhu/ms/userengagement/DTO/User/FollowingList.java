package com.unhu.ms.userengagement.DTO.User;

import java.util.List;

public class FollowingList {
    List<String> followingList;


    public List<String> getFollowingList() {
        return followingList;
    }

    public void setFollowingList(List<String> followingList) {
        this.followingList = followingList;
    }

}
