package com.unhu.ms.userengagement.Resource;

import com.unhu.ms.userengagement.DTO.TestDTO;
import com.unhu.ms.userengagement.ServiceImpl.TestServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/unhu/api/v1", path = "/unhu/api/v1")
public class TestAPIController {

    @GetMapping("/test")
    public TestDTO greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        TestServiceImpl testService = new TestServiceImpl();
        return testService.testFunction(name);
    }
}
