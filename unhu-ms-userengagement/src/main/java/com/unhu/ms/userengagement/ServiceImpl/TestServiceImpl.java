package com.unhu.ms.userengagement.ServiceImpl;

import com.unhu.ms.userengagement.DTO.TestDTO;
import com.unhu.ms.userengagement.Service.TestService;

import java.util.concurrent.atomic.AtomicLong;


public class TestServiceImpl implements TestService {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    public TestServiceImpl(){

    }

    @Override
    public TestDTO testFunction(String name) {
        return new TestDTO(counter.incrementAndGet(), String.format(template, name));
    }
}
