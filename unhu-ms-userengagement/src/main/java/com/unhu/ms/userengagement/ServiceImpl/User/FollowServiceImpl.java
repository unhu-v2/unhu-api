package com.unhu.ms.userengagement.ServiceImpl.User;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unhu.ms.userengagement.DTO.ResponseDTO;
import com.unhu.ms.userengagement.Service.User.FollowService;
import com.unhu.ms.userengagement.Utils.UpdateOperationMongoDb;


import java.util.HashMap;
import java.util.Map;

public class FollowServiceImpl implements FollowService {

    @Override
    public ResponseDTO addFollower(String username, String followingUsername) {
        String message = "";
        String responseCode = "";

        //Add the following user to following list
        Map<String, Object> mapFollowing = new HashMap<>();
        mapFollowing.put("username", followingUsername);

        //update the follower count of the person being followed
        Map<String, Object> mapFollower = new HashMap<>();
        mapFollower.put("username", username);


        UpdateOperationMongoDb following = new UpdateOperationMongoDb("unhu_users");
        if(!following.addUserFollowing(username, followingUsername)){
            message = "Unable to update the following count and following list";
            responseCode = "500";
            return new ResponseDTO(message, responseCode);
        }

        UpdateOperationMongoDb follower = new UpdateOperationMongoDb("unhu_users");
        if(follower.addUserFollower(followingUsername, username)){
            message = "Updated the following and follower properties of respective users";
            responseCode = "200";
        }
        else {
            message = "Unable to follow the profile";
            responseCode = "500";
        }

        return new ResponseDTO(message, responseCode);
    }

    @Override
    public ResponseDTO removeFollower(String username, String followingUsername) {
        String message = "";
        String responseCode = "";

        //Remove the following user to following list
        Map<String, Object> mapFollowing = new HashMap<>();
        mapFollowing.put("username", followingUsername);

        //update the follower count of the person being followed
        Map<String, Object> mapFollower = new HashMap<>();
        mapFollower.put("username", username);


        // Remove the following from user's following list
        UpdateOperationMongoDb following = new UpdateOperationMongoDb("unhu_users");
        if(!following.removeUserFollowing(username, followingUsername)){
            message = "Unable to update the following count and following list";
            responseCode = "500";
            return new ResponseDTO(message, responseCode);
        }

        // Remove the follwer from end user's following list
        UpdateOperationMongoDb follower = new UpdateOperationMongoDb("unhu_users");
        if(follower.removeUserFollower(followingUsername, username)){
            message = "Updated the following and follower properties of respective users";
            responseCode = "200";
        }
        else {
            message = "Unable to follow the profile";
            responseCode = "500";
        }

        return new ResponseDTO(message, responseCode);
    }


    public Map<String, Object> getMap(Object object){
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> map = oMapper.convertValue(object, Map.class);
        return map;
    }
}
