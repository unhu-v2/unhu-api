package com.unhu.ms.userengagement.Utils;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;


public class InsertOperationMongoDb{

    private final static Logger LOGGER = Logger.getLogger(InsertOperationMongoDb.class.getName());


    //Insert document to Collection
    public boolean insertDataMongoDb(Map<String, Object> data, String mongoCollection) throws IOException {
        MongoDbConnection mongoDbConnection = new MongoDbConnection();
        MongoDatabase mongoDatabase = mongoDbConnection.createConnection();

        MongoCollection<Document> collection = mongoDatabase.getCollection(mongoCollection);
        Document document = new Document(data);

        SearchOperationMongoDb searchOperationMongoDb = new SearchOperationMongoDb();
        if(!searchOperationMongoDb.searchForExistingProfile(data, mongoCollection)){
            LOGGER.info("Record Exists in the Database");
            return false;
        }
        collection.insertOne(document);
        mongoDbConnection.close();
        return true;
    }
}
