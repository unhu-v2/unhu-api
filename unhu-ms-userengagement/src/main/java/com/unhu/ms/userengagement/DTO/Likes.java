package com.unhu.ms.userengagement.DTO;

import java.util.List;

public class Likes {
    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    public String likes_count;

    public List<String> getLiked_by() {
        return liked_by;
    }

    public void setLiked_by(List<String> liked_by) {
        this.liked_by = liked_by;
    }

    public List<String> liked_by;
}
