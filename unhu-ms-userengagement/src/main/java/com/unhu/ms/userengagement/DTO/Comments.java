package com.unhu.ms.userengagement.DTO;

import java.util.List;

public class Comments {
    public String getComment_likes() {
        return comment_likes;
    }

    public void setComment_likes(String comment_likes) {
        this.comment_likes = comment_likes;
    }

    public String comment_likes;

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String comment_count;

    public List<CommentDescription> getComment_description() {
        return comment_description;
    }

    public void setComment_description(List<CommentDescription> comment_description) {
        this.comment_description = comment_description;
    }

    public List<CommentDescription> comment_description;
}
