package com.unhu.ms.userengagement.ServiceImpl.User;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unhu.ms.userengagement.DTO.Likes;
import com.unhu.ms.userengagement.DTO.ResponseDTO;
import com.unhu.ms.userengagement.Service.User.LikeService;
import com.unhu.ms.userengagement.Utils.UpdateOperationMongoDb;


import java.util.Map;

public class LikeServiceImpl implements LikeService {

    @Override
    public ResponseDTO addLike(String postUid, String username) {
        String message = "";
        String responseCode = "";
        UpdateOperationMongoDb mongoDbOperations = new UpdateOperationMongoDb("unhu_posts");
        if(mongoDbOperations.addPostLike(postUid, username)){
            message = "Liked the post successfully";
            responseCode = "200";
        }
        else{
            message = "Issue with liking the post";
            responseCode = "500";
        }
        return new ResponseDTO(message, responseCode);
    }

    @Override
    public ResponseDTO removeLike(String postUid, String username) {
        String message = "";
        String responseCode = "";
        UpdateOperationMongoDb mongoDbOperations = new UpdateOperationMongoDb("unhu_posts");
        if(mongoDbOperations.removePostLike(postUid, username)){
            message = "Unliked the post successfully";
            responseCode = "200";
        }
        else{
            message = "Issue with unliking the post";
            responseCode = "500";
        }
        return new ResponseDTO(message, responseCode);
    }

    public Likes getLikesAndLikeList(String postUid){
        String message = "";
        String responseCode = "";
        UpdateOperationMongoDb mongoDbOperations = new UpdateOperationMongoDb("unhu_posts");
        Likes likes = mongoDbOperations.getLikesAndLikesList(postUid);
        return likes;
    }

    public Map<String, Object> getMap(Object object){
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> map = oMapper.convertValue(object, Map.class);
        return map;
    }
}
