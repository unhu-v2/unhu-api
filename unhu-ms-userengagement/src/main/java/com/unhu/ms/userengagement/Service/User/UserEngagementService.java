package com.unhu.ms.userengagement.Service.User;

import com.unhu.ms.userengagement.DTO.User.LoginDTO;
import com.unhu.ms.userengagement.DTO.User.RegistrationDTO;
import com.unhu.ms.userengagement.DTO.User.UpdatePasswordDTO;
import com.unhu.ms.userengagement.DTO.ResponseDTO;

import java.io.IOException;

public interface UserEngagementService {
    public ResponseDTO registerUser(RegistrationDTO registrationDTO) throws IOException;
    public ResponseDTO login(LoginDTO registrationDTO) throws IOException;
    public ResponseDTO changePassword(UpdatePasswordDTO updatePasswordDTO);
}
