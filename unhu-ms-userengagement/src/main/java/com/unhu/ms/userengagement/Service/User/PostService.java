package com.unhu.ms.userengagement.Service.User;


import com.unhu.ms.userengagement.DTO.PostDTO;
import com.unhu.ms.userengagement.DTO.ResponseDTO;

public interface PostService {
    public ResponseDTO addPost(PostDTO data);
}
