package com.unhu.ms.userengagement.DTO.User;

import com.mongodb.lang.NonNull;

public class UpdatePasswordDTO {
    @NonNull
    private String password;
    @NonNull
    private String email;

    @NonNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }
}
