package com.unhu.ms.userengagement.DTO.User;

import com.mongodb.lang.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RegistrationDTO{

    @NonNull
    public String fullname;
    @NonNull
    public String email_id;
    @NonNull
    public String username;
    @NonNull
    public String password;
    public String user_since;
    public Shelf shelf;
    public Posts posts;
    public String profile_picture;
    public Followers followers;
    public String bio;
    public String link;

    @NonNull
    public String getFullname() {
        return fullname;
    }

    public void setFullname(@NonNull String fullname) {
        this.fullname = fullname;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(@NonNull String email_id) {
        this.email_id = email_id;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSmarthub_link() {
        return smarthub_link;
    }

    public void setSmarthub_link(String smarthub_link) {
        this.smarthub_link = smarthub_link;
    }

    public String smarthub_link;

    public Following getFollowing() {
        return following;
    }

    public void setFollowing(Following following) {
        this.following = following;
    }

    public Following following;

    public Followers getFollowers() {
        return followers;
    }

    public void setFollowers(Followers followers) {
        this.followers = followers;
    }

    public String getUser_since() {
        return user_since;
    }

    public void setUser_since(String user_since) {
        this.user_since = user_since;
    }
    public Shelf getShelf() {
        return shelf;
    }

    public void setShelf(Shelf shelf) {
        this.shelf = shelf;
    }

    public Posts getPosts() {
        return posts;
    }

    public void setPosts(Posts posts) {
        this.posts = posts;
    }
    @NonNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NonNull String username) {
        this.username = username;
    }
}
