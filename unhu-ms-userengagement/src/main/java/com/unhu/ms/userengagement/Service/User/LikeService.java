package com.unhu.ms.userengagement.Service.User;


import com.unhu.ms.userengagement.DTO.Likes;
import com.unhu.ms.userengagement.DTO.ResponseDTO;

public interface LikeService {
    public ResponseDTO addLike(String post_uid, String username);
    public ResponseDTO removeLike(String post_uid, String username);
    public Likes getLikesAndLikeList(String post_uid);
}
