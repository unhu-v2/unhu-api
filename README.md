**Unhu Backend APIs**

---

## Pre Requisites
1. MongoDB server running
2. Open MongoCli and run following commands to setup mongo environment:
    use unhu
    db.createCollection("unhu_users", { capped : true, size : 5242880, max : 5000 } )
    db.createCollection("unhu_posts", { capped : true, size : 5242880, max : 5000 } )

## Building and running the jar to deploy the microservice

1. Run mvn clean package in Project directory
2. run java -jar target/unhu-user-ms-docker.jar --spring.config.location=src/main/resources/application.properties
3. Open url in Chrome for documentation: http://localhost:8080/swagger-ui.html#/



---

