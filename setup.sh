#Build docker images for all the microservices
#docker build -f unhu-ms-commentservice/Dockerfile -t unhu-commentservice-ms-api .
#docker build -f unhu-ms-followservice/Dockerfile -t unhu-followservice-ms-api .
#docker build -f unhu-ms-likeservice/Dockerfile -t unhu-likeservice-ms-api .
#docker build -f unhu-ms-postservice/Dockerfile -t unhu-postservice-ms-api .
docker build -f unhu-ms-userengagement/Dockerfile -t unhu-user-ms-api .

#Build a docker container for mongo along with all the other microservices
docker-compose up

